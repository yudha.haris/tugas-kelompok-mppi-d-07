"""tkptspbp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
import temancovid.urls as teman_covid
import sharepengalaman.urls as share_pengalaman
import userprofile.urls as userprofile
from django.conf import settings
from django.conf.urls.static import static
import wash_your_lyrics.urls as lyrics
from comment_pengalaman.views import detail
import Obat.urls as obat
import homepage.urls as index
import register.urls as register

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(index)),
    path('story/', include(share_pengalaman)),
    path('user-profile/', include(userprofile)),
    path("wash-your-lyrics/", include(lyrics)),
    path('<slug:slug>/', detail, name="detail"),
    path('informasi-obat/', include(obat)),
    path('register/', include(register)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# settings.DEBUG obtained from https://github.com/CoreyMSchafer/
