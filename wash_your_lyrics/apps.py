from django.apps import AppConfig


class WashYourLyricsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wash_your_lyrics'
