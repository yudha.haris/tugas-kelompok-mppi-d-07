from django.urls import path
from .views import index, feedbacks, create_post

urlpatterns = [
    path('', index, name='index'),
    #path('feedback/', feedbacks, name='feedback'),
    path('feedback/', create_post, name='create_post')
]
