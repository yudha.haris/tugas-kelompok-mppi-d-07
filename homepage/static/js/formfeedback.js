$('#feedbackform').on('submit', function (event) {
    event.preventDefault()
    // console.log("form submitted!")
    create_post()
})

function create_post() {
    // console.log("create post is working!")
    // console.log($('#post-text').val()) // ngambil yang ada di text fieldnya terus ditampilin

    $.ajax({
        url : "", // the endpoint
        type : "POST", // http method
        data : {
            the_post : $('#post-text').val(), // data sent with the post request
        },

        // handle a successful response
        success : function(response_data) {
            $('#post-text').val(''); // remove the value from the input
            // console.log(response_data); // log the returned json to the console
            // console.log("success"); // another sanity check
            alert("Terima kasih! Feedback kamu sudah kami terima :D")
        },

        // handle a non-successful response
        error : function() {
            // console.log("masuk ke error")
            alert("Maaf nih kayaknya ada yang error :(")
        }
    });
}