from django.apps import AppConfig


class CommentPengalamanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'comment_pengalaman'
